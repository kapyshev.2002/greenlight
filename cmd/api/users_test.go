package main

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRegisterUserHandler(t *testing.T) {
	// Initialize the application
	app := newTestApplication(t)

	// Create a JSON payload for the request
	payload := struct {
		Name     string `json:"name"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}{
		Name:     "John Doe",
		Email:    "johndoe@example.com",
		Password: "password123",
	}
	payloadBytes, err := json.Marshal(payload)
	assert.NoError(t, err)

	req, err := http.NewRequest("POST", "/api/users", bytes.NewReader(payloadBytes))
	assert.NoError(t, err)

	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(app.registerUserHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)

	var response map[string]interface{}
	err = json.Unmarshal(rr.Body.Bytes(), &response)
	assert.NoError(t, err)

	user, ok := response["user"].(map[string]interface{})
	assert.True(t, ok)
	assert.Equal(t, payload.Name, user["name"])
	assert.Equal(t, payload.Email, user["email"])
	assert.Equal(t, false, user["activated"])
}

func TestActivateUserHandler(t *testing.T) {
	app := newTestApplication(t)

	requestBody := map[string]string{
		"token": "valid_token",
	}
	requestBodyBytes, _ := json.Marshal(requestBody)

	req, err := http.NewRequest("POST", "/api/v1/activate", bytes.NewBuffer(requestBodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	app.activateUserHandler(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expectedResponseBody := `{"user":{"id":1,"email":"test@example.com","activated":true}}`
	if rr.Body.String() != expectedResponseBody {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expectedResponseBody)
	}
}

package main

import (
	"expvar"
	"github.com/stretchr/testify/assert"
	"greenlight.bcc/internal/data"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func TestRecoverPanic(t *testing.T) {
	app := new(application)

	ts := httptest.NewServer(app.recoverPanic(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("test panic")
	})))
	defer ts.Close()

	resp, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()

	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusInternalServerError {
		t.Errorf("expected status code %d but got %d", http.StatusInternalServerError, resp.StatusCode)
	}
}

func TestMetrics(t *testing.T) {
	app := &application{}

	handler := app.metrics(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	// verify that expvar counters have been incremented
	totalRequestsReceived := expvar.Get("total_requests_received").(*expvar.Int)
	assert.Equal(t, int64(1), totalRequestsReceived.Value())

	totalResponsesSent := expvar.Get("total_responses_sent").(*expvar.Int)
	assert.Equal(t, int64(1), totalResponsesSent.Value())

	totalProcessingTimeMicroseconds := expvar.Get("total_processing_time_μs").(*expvar.Int)
	assert.True(t, totalProcessingTimeMicroseconds.Value() > 0)

	totalResponsesSentByStatus := expvar.Get("total_responses_sent_by_status").(*expvar.Map)
	statusKey := strconv.Itoa(http.StatusOK)
	statusValue := totalResponsesSentByStatus.Get(statusKey)
	assert.NotNil(t, statusValue, "status key %s not found in total_responses_sent_by_status", statusKey)
	assert.IsType(t, &expvar.Int{}, statusValue)
	assert.Equal(t, int64(1), statusValue.(*expvar.Int).Value())
}

func TestRequireAuthenticatedUser(t *testing.T) {
	// Set up a fake HTTP server
	app := &application{} // Replace with your application struct
	handlerCalled := false
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerCalled = true
	})
	fakeServer := httptest.NewServer(app.requireAuthenticatedUser(handler))
	defer fakeServer.Close()

	req, err := http.NewRequest("GET", fakeServer.URL, nil)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusUnauthorized {
		t.Errorf("Expected status code %d, but got %d", http.StatusUnauthorized, resp.StatusCode)
	}
	if handlerCalled {
		t.Error("Handler should not be called when user is not authenticated")
	}

	reqWithAuth, err := http.NewRequest("GET", fakeServer.URL, nil)
	if err != nil {
		t.Fatal(err)
	}
	reqWithAuth.AddCookie(&http.Cookie{Name: "auth_token", Value: "example"})
	respWithAuth, err := http.DefaultClient.Do(reqWithAuth)
	if err != nil {
		t.Fatal(err)
	}
	if respWithAuth.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, but got %d", http.StatusOK, respWithAuth.StatusCode)
	}
	if !handlerCalled {
		t.Error("Handler should be called when user is authenticated")
	}
}

func TestAuthenticateWithInvalidToken(t *testing.T) {
	app := &application{}

	token := "invalid_token"

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set("Authorization", "Bearer "+token)

	rr := httptest.NewRecorder()

	app.authenticate(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// This handler should never be called because the token is invalid
		t.Errorf("authenticate called the next handler with an invalid token")
	})).ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}
func TestAuthenticateWithMissingToken(t *testing.T) {
	app := &application{}

	req := httptest.NewRequest(http.MethodGet, "/", nil)

	rr := httptest.NewRecorder()

	app.authenticate(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := app.contextGetUser(r)
		if user == nil || user.ID != data.AnonymousUser.ID {
			t.Errorf("authenticate failed to set the correct user in the request context")
		}
	})).ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
